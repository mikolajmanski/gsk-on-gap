import org.apache.sling.api.resource.ResourceResolver

// Configuration
setDryRun false
setActivate false
setSilent true

def oldType = "accelerator/amp-core/components/conf"
def newType = "accelerator/resource-script-mapper/config"

def setNewResourceType(Resource ampConfigResource, String newType) {
	println "Setting new resource type for the configuration node"
	def props = ampConfigResource.getChild('jcr:content').adaptTo(ModifiableValueMap.class)
	props.put("sling:resourceType", newType)
}

def moveFromSettingsToSlingConfigs(Resource ampConfigResource, Resource settingsResource) {
	def slingConfigsResource = ResourceUtil.getOrCreateResource(resourceResolver, settingsResource.parent.path + '/sling:configs', settingsResource.valueMap, null, false)
	def newConfigResourcePath = slingConfigsResource.path + '/' + ampConfigResource.name

	if (isNewLocationEmpty(newConfigResourcePath)) {
		println "Moving config resource to new location"
		session.move(ampConfigResource.path, newConfigResourcePath)
	}
	else {
		println "ERROR: Config node already exists at the specified location: " + newConfigResourcePath
		println "Aborting script execution."
	}

}

def isNewLocationEmpty(String path) {
	return resourceResolver.getResource(path) == null
}

// Execution

def it = resourceResolver.findResources("SELECT * FROM [cq:Page] AS s WHERE ISDESCENDANTNODE('/conf') AND [jcr:content/sling:resourceType] = '${oldType}'", "JCR-SQL2")

while (it.hasNext()) {
	def ampConfigResource = it.next()
	def settingsResource = ampConfigResource.getParent()
	setNewResourceType(ampConfigResource, newType)
	moveFromSettingsToSlingConfigs(ampConfigResource, settingsResource)

	if (!dryRun) {
		println "Saving script results"
		session.save()
	}
	else {
		println "Dry run. Changes won't be saved to JCR."
	}
}