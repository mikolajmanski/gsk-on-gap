/*
 * ZGCQ-7945 - Migrates RTE hyperlinks and selection styles from Site Group configuration to the Variant Console
 */

setDryRun false
setActivate false
setSilent true

def contentPath = "/content"
def variantsRoot = "/etc/variants"
def siteGroupRenderer = "zg/zenGarden/renderers/siteGroupRenderer"
def mobileSiteGroupRenderer = "zg/zenGarden/renderers/mobileGroupRenderer"

def siteGroupsQuery = "SELECT * FROM [nt:unstructured] WHERE ISDESCENDANTNODE('$contentPath') AND [sling:resourceType] IN ('$siteGroupRenderer','$mobileSiteGroupRenderer')"
def variantsConsolesQuery = "SELECT * FROM [nt:unstructured] WHERE ISDESCENDANTNODE('$variantsRoot') AND [sling:resourceType] = 'accelerator/variants/renderers/variantsConsole'"

consoleStore = [:].withDefault { [:].withDefault { [:] } }
consoles = resourceResolver.findResources(variantsConsolesQuery, "JCR-SQL2").toList()

process(siteGroupsQuery, { resource, valueMap ->
	def siteGroupPath = resource.parent.path

	def hyperlinkStyles = getResource(siteGroupPath + "/configuration/styles-configuration/hyperlink-predefined-styles/jcr:content/cssClasses/options")
	def selectionStyles = getResource(siteGroupPath + "/configuration/styles-configuration/rich-text-selection-predefined-styles/jcr:content/cssClasses/options")

	if (hasChildren(hyperlinkStyles) || hasChildren(selectionStyles)) {

		def console = getBestMatchingConsole(siteGroupPath)
		if (console) {
			def consolePagePath = console.parent.path
			info("Starting migration to variants console: '$consolePagePath'")

			migrateStyles(consolePagePath, "selectionStyles", "accelerator/variants/components/selectionStyles", selectionStyles)
			migrateStyles(consolePagePath, "hyperlinkStyles", "accelerator/variants/components/hyperlinkStyles", hyperlinkStyles)
		} else {
			info("WARN: There is no variant console matching site group: '$siteGroupPath' - Styles will not be migrated." +
					" Please configure a variants console with a path matching this site group and re-run the script.")
			return false
		}
	} else {
		info("No hyperlink/selection styles were found in site group.")
	}
	removeOldStylesPages(siteGroupPath)
	return true
})

def hasChildren(resource) {
	return resource && resource.listChildren().hasNext()
}

def removeOldStylesPages(siteGroupPath) {
	removeResource(siteGroupPath + "/configuration/styles-configuration/hyperlink-predefined-styles")
	removeResource(siteGroupPath + "/configuration/styles-configuration/rich-text-selection-predefined-styles")
}

def getBestMatchingConsole(pagePath) {
	def foundConsole = null
	def foundPath = null

	consoles.each { console ->
		def path = console.paths
				.findAll { pagePath.startsWith(it) }
				.max { it.length() }

		if (path) {
			def isBetterMatch = !foundPath || path.length() > foundPath.length()
			if (isBetterMatch) {
				foundPath = path
				foundConsole = console
			}
		}
	}
	return foundConsole
}

def migrateStyles(consolePath, variantsPageName, componentType, oldStylesPage) {
	if (hasChildren(oldStylesPage)) {
		def variantsPage = getPage(consolePath + "/" + variantsPageName)
		if (!variantsPage) {
			variantsPage = pageManager.create(consolePath, variantsPageName, "/apps/accelerator/variants/templates/variantsPage", null, false)
			variantsPage.contentResource.componentType = componentType
		}

		oldStylesPage.listChildren().each { style ->
			if (!isLightboxHyperlinkStyle(variantsPageName, style)) {
				def properties = ['id'                : style.id,
								  'cssClass'          : style.value,
								  'title'             : style.title ?: style.value,
								  'sling:resourceType': 'accelerator/variants/components/variantConfiguration']

				if (!consoleStore[consolePath][variantsPageName][style.value]) {
					try {
						resourceResolver.create(variantsPage.contentResource.getChild("variants"), properties.id, properties)
					} catch (org.apache.sling.api.resource.PersistenceException e) {
						info(e.getMessage())
					}
					consoleStore[consolePath][variantsPageName][style.value] = '1'
				} else {
					info("Style $style.value already added to $consolePath : $variantsPageName")
				}
			} else {
				info("Found a hyperlink style with 'lightbox' CSS class - will not be migrated")
			}
		}
	}
}

def isLightboxHyperlinkStyle(variantsPageName, style) {
	return variantsPageName == "hyperlinkStyles" && style.value == "lightbox"
}

def info(msg) {
	if (!silent) {
		println msg
	}
}