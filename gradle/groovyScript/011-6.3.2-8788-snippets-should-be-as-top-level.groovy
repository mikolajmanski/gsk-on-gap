/*
 * ZGCQ-8788 Snippets should be as top level container
 */

setDryRun false
setActivate false
setSilent true

def query = "SELECT childContent.* " +
		"FROM [cq:Page] AS child " +
		"INNER JOIN [cq:Page] AS parent " +
		"ON ISCHILDNODE(child, parent) " +
		"INNER JOIN [cq:PageContent] AS childContent " +
		"ON ISCHILDNODE(childContent, child) " +
		"WHERE ISDESCENDANTNODE(parent, [/content]) " +
		"AND NAME(child) IN ('snippet','overlay') " +
		"AND NAME(parent) = 'content' " +
		"AND NAME(childContent) = 'jcr:content' " +
		"AND childContent.[cq:template] = '/apps/zg/commons/templates/base/snippetContentsTemplate'"

process(query, { resource, valueMap ->
	valueMap.put('cq:template', '/apps/zg/commons/templates/base/topLevel/snippetContentsTemplate')
})

