/*
* ZGCQ-7907 - Social tags should inherit default values
* This script moves twitterSite property to twitter social metatags
*/

setDryRun false
setActivate false
setSilent true

def CONTENT_PATH = "/content"
def PROPERTY_NAME = "jcr:content/twitterSite"
def QUERY = "SELECT * FROM [cq:Page] WHERE ISDESCENDANTNODE('$CONTENT_PATH') AND [$PROPERTY_NAME] IS NOT NULL"

def toRemove = []

process(QUERY, { resource, valueMap ->
    def path = resource.getPath()
    if (!silent) {
        println "Moving '$path/$PROPERTY_NAME' to '$path/jcr:content/twitterMetatags/item_0'"
    }

    def builder = getResourceBuilder(path + "/jcr:content")
    builder.twitterMetatags {[
            "item_0"([
                    'key'  : 'twitter:site',
                    'value': valueMap.get(PROPERTY_NAME)
            ])
    ]}
    toRemove << ("$path/$PROPERTY_NAME")
})

if (!toRemove.empty) {
    removeResources(toRemove)
} else if (!silent) {
    println("Nothing has changed!")
}