/**
 * ZGCQ-10369 - Extract Resource Script Mapper module from Amp Core
 *
 * This script will update config for AMP
 */

setDryRun false
setActivate false
setSilent true

def query = "SELECT * FROM [nt:unstructured] WHERE ISDESCENDANTNODE([/conf]) AND [sling:resourceType]='accelerator/amp-core/components/conf'"

setResourceProperties query, ['sling:resourceType':'accelerator/resource-script-mapper/config'], true