/*
* ZGCQ-7861 - Variant group structure change
*/

setDryRun false
setActivate false
setSilent true

def contentPath = '/etc/variants'
def query = "SELECT * FROM [nt:unstructured] WHERE ISDESCENDANTNODE('$contentPath') AND [sling:resourceType] = 'accelerator/variants/renderers/variantsGroup'"

process(query, { resource, valueMap ->
    resource?.getChild('variants').getChildren().each{ r ->
        def title = resource.getValueMap().get('jcr:title', String.class)
        def path = resource.getParent().getParent().getPath()
            ModifiableValueMap map = r.adaptTo(ModifiableValueMap.class)
            if (title != null) {
                map.put('groupName', title)
                resourceResolver.move(r.getPath(), path + '/jcr:content/variants')
            }
    }

    resourceResolver.delete(resource.getParent())
    if(!dryRun) {
        session.save()
    }
})
