#!/bin/bash

if [[ $# -ne 3 || $1 == "-h" ]]; then
    echo "Usage: force-recompilation.sh <crx-quickstart-dir> <http://server:port> <user:password>"
    echo
    echo "Example arguments: crx-quickstart/ http://localhost:4502 admin:admin"
    echo
    echo "This will force a recompilation of all Sling scripts (jsps, java, sightly etc.)."
    exit
fi

dir="$1"
host="$2"
user="$3"

if [ ! -d "$dir/../crx-quickstart" ]; then
    echo "No crx-quickstart directory found."
    exit
fi

echo "stopping the org.apache.sling.commons.fsclassloader bundle..."
bundle1="org.apache.sling.commons.fsclassloader"
curl -f -s -S -u $user -F action=stop  $host/system/console/bundles/$bundle1 > /dev/null
exitCode=$?
if [ $exitCode -ne 0 ]; then
    echo
    if [ $exitCode -eq 22 ]; then
        echo "Invalid admin password."
    fi
    echo "aborted.";
    exit
fi

echo "stopping the com.adobe.granite.ui.clientlibs bundle..."
bundle2="com.adobe.granite.ui.clientlibs"
curl -f -s -S -u $user -F action=stop  $host/system/console/bundles/$bundle2 > /dev/null
exitCode=$?
if [ $exitCode -ne 0 ]; then
    echo
    if [ $exitCode -eq 22 ]; then
        echo "Invalid admin password."
    fi
    echo "aborted.";
    exit
fi

cd "$dir"

classesDir=`find launchpad/felix -path "*/bundle*/data/classes" -type d`
if [ -d "$classesDir" ]; then
    echo "deleting file sytem classes cache: `pwd`/$classesDir ..."
    rm -rf "$classesDir"
else
    echo "file system classes cache empty or already cleared"
fi


echo "starting the org.apache.sling.commons.fsclassloader bundle..."
curl -f -s -S -u $user -F action=start $host/system/console/bundles/$bundle1 > /dev/null
if [ $? -ne 0 ]; then
    echo
    echo "aborted, bundle was likely not restarted.";
    exit
fi

classesDir=`find launchpad/felix -path "*/bundle*/data/outputcache" -type d`
if [ -d "$classesDir" ]; then
    echo "deleting file sytem classes cache: `pwd`/$classesDir ..."
    rm -rf "$classesDir"
else
    echo "file system classes cache empty or already cleared"
fi


echo "starting the com.adobe.granite.ui.clientlibs bundle..."
curl -f -s -S -u $user -F action=start $host/system/console/bundles/$bundle2 > /dev/null
if [ $? -ne 0 ]; then
    echo
    echo "aborted, bundle was likely not restarted.";
    exit
fi
echo "done."
