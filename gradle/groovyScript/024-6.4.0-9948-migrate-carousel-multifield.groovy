/*
 * ZGZGCQ-9948 Saved multifield values are empty after reopening the dialog
 */

import org.apache.sling.api.resource.Resource

import java.util.regex.Matcher
import java.util.regex.Pattern

setDryRun false
setActivate false
setSilent true

def query = "SELECT * FROM [nt:unstructured] WHERE [sling:resourceType] = 'zg/commons/components/media/carousel'"
Pattern NAME_ITEM_PATTERN = Pattern.compile("item_([0-9]*)")
Pattern SLIDE_ITEM_PATTERN = Pattern.compile("slides-item_([0-9]*)")


Iterator<Resource> findResources = resourceResolver.findResources(query, "sql")
while (findResources.hasNext()) {
	Resource resource = findResources.next()
	Resource names = resource.getChild("names")
	if (names != null) {
		moveChildren(names, NAME_ITEM_PATTERN)
	}
	moveChildren(resource, SLIDE_ITEM_PATTERN)
	if (!dryRun) {
		session.save()
	}
}

def moveChildren(resource, PATTERN) {
	Iterator<Resource> children = resource.listChildren()
	while(children.hasNext()) {
		Resource child = children.next()
		String nodeName = child.getName()
		Matcher matcher = PATTERN.matcher(nodeName)
		if(matcher.find()) {
			String counter = matcher.group(1)
			String sourcePath = child.getPath()
			String destinationPath = sourcePath.substring(0, sourcePath.lastIndexOf("_")) + counter
			session.move(sourcePath, destinationPath)
		}
	}
}