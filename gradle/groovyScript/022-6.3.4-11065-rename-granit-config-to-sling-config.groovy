setDryRun false
setActivate false
setSilent true

def JCR_CONTENT_CQ_CONF = "jcr:content/cq:conf"
def SLING_CONFIG_REF = "sling:configRef"
def CQ_CONF = "cq:conf"

def contentPath = "/content"
def query = "SELECT * FROM [cq:Page] WHERE ISDESCENDANTNODE('$contentPath') AND [$JCR_CONTENT_CQ_CONF] IS NOT NULL"

process(query, { resource, valueMap ->
	def contentResourcePath = resource.adaptTo(Page.class).contentResource.path
	if (!silent) {
		println(contentResourcePath + " -> " + valueMap.get(JCR_CONTENT_CQ_CONF))
	}
	setResourceProperties([contentResourcePath], ["$SLING_CONFIG_REF": valueMap.get(JCR_CONTENT_CQ_CONF)])
	removeExistingProperties([contentResourcePath], ["$CQ_CONF"])
})