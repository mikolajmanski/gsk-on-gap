/*
 * ZGCQ-8874 Composite as compat component
 */

import org.apache.sling.api.resource.ModifiableValueMap

setDryRun false
setActivate false
setSilent true

def resource = getResource("/etc/zg/config/compat/jcr:content/compatConfig")
if (resource) {
    items = resource.valueMap.get("items", String[].class) as List
    items << "/apps/accelerator/composites/components/composite"
    setResourceProperties([resource.path], ["items": items.toArray()])
}
