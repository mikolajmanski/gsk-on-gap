/*
* ZGCQ-8871 - Make `File List Items` dam ordered.
*
* Before `File List Items` were ordered by their order in CRX. In 6.3.2 they are ordered by `dc:title` property by default.
* Use this script to order `File List Items` back to their CRX order.
*
*/

setDryRun false
setActivate false
setSilent true

def contentPath = "/content"
def query = "SELECT * FROM [nt:unstructured] WHERE ISDESCENDANTNODE('$contentPath') AND [sling:resourceType] = 'zg/commons/components/data/fileList'"

process(query, { resource, valueMap ->
	valueMap << ["order": "dam"]
})