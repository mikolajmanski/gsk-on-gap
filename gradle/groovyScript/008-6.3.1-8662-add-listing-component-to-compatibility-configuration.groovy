import org.apache.sling.api.resource.ModifiableValueMap

setDryRun false
setActivate false
setSilent true

def resource = getResource("/etc/zg/config/compat/jcr:content/compatConfig")
if (resource) {
    items = resource.valueMap.get("items", String[].class) as List
    items << "/apps/bridge/components/listing"
    setResourceProperties([resource.path], ["items": items.toArray()])
}
