/*
* PES-536 Sets default header size for Article List and File List components
* to prevent unwanted changes when submitting component dialog
*/

setDryRun false
setActivate false

def titleTypePropertyName = "titleType"
def defaultTitleType = "H3"

def RESOURCE_TYPES = [ "zg/commons/components/publication/articleList", "zg/commons/components/data/fileList" ]

SESSION_SAVE_INTERVAL = 1000

RESOURCE_TYPES.each{ resourceType ->
    def query = "SELECT * FROM [nt:unstructured] WHERE (ISDESCENDANTNODE('/content') OR ISDESCENDANTNODE('/conf')) AND [sling:resourceType]='$resourceType'"
    def counter = 1

    process(query, {resource, valueMap ->
        if (!valueMap.containsKey(titleTypePropertyName)) {
            valueMap.put(titleTypePropertyName, defaultTitleType)

            if (!dryRun && shouldSaveSession(counter)) {
                println "Saving changes after processing ${counter} items"
                resourceResolver.commit()
            }
            counter++
            return true
        }
    })
}

def shouldSaveSession(counter) {
    return counter % SESSION_SAVE_INTERVAL == 0
}
