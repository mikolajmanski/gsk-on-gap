plugins {
    id("com.neva.fork")
    id("com.cognifide.aem.instance")
}

apply(from = "gradle/fork.gradle.kts")

description = "GSK - Common Framework"
version = project.property("cf-env-version") ?: throw Exception("version required")

repositories {
    jcenter()
    maven { url = uri("https://repo.adobe.com/nexus/content/groups/public") }
    maven { url = uri("https://repo1.maven.org/maven2") }
    maven { url = uri("https://repo1.maven.org/maven2") }
    maven { url = uri("https://dl.bintray.com/neva-dev/maven-public") }
}

aem {

    tasks {

        register<Exec>("pasat") {
            commandLine = listOf("./pasat/run-pasat")
            dependsOn("instanceSetup")
            finalizedBy("instanceAwait")
        }

        instanceTail {
            tailer {
                linesChunkSize = 4000
            }
        }

        register("groovy") {
            doLast {
                syncInstances {
                    groovyConsole.evalScripts(fileTree("gradle/groovyScript"))
                }
            }
            mustRunAfter("pasat")
        }

        instanceSatisfy {
            packages {
                group("debug-slice") {
                    download("https://repo1.maven.org/maven2/com/google/inject/guice/3.0/guice-3.0-no_aop.jar")
                }
                group("adobe-day-care") {
                    download("./gradle/instance/page-properties-warning-message-fix.zip")
                }
            }
        }

        instanceProvision {
            step("enable-crxde") {
                description = "Enables CRX DE"
                condition { once() && instance.environment != "prod" }
                action {
                    sync {
                        osgiFramework.configure("org.apache.sling.jcr.davex.impl.servlets.SlingDavExServlet", mapOf(
                                "alias" to "/crx/server"
                        ))
                    }
                }
            }
        }
    }
}
