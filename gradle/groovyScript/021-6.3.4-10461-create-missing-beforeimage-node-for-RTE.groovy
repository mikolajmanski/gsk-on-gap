import javax.jcr.query.Query

/*
* ZGCQ-10461 script will add beforeImage node to RTE components on author to prevent 404.
*
*/

setDryRun false
setActivate false
setSilent true

def PATH = '/content'
def index = 0

def query = "SELECT * FROM [nt:unstructured] WHERE ISDESCENDANTNODE ([$PATH]) AND [sling:resourceType] = 'zg/commons/components/base/richText'"
resourceResolver.findResources(query, Query.JCR_SQL2).each { resource ->
	def node = resource.adaptTo(Node.class)

	if (!node.hasNode("beforeImage")) {
		node.addNode("beforeImage", "nt:unstructured")
		def beforeImageNode = node.getNode("beforeImage")
		beforeImageNode.setProperty("sling:resourceType", "foundation/components/image")
		index++
	}
	if (!dryRun && index % 800 == 0) {
		session.save()
	}
}
if (!dryRun) {
	session.save()
}

