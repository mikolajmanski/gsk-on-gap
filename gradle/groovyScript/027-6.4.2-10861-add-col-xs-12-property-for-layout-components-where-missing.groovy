/*
 * ZGCQ-10861 - Add missing col-xs="12" property for layout components
 */

setDryRun false
setActivate false
setSilent true

def contentPath = '/content/'

def COL_XS = 'col-xs'
def FULL_WIDTH = '12'

def query = "SELECT * " +
        "FROM [nt:unstructured] AS item " +
        "WHERE ISDESCENDANTNODE(item, [$contentPath]) AND " +
        "(item.[sling:resourceType] = 'zg/layoutEngine/bootstrapGrid/components/container' OR " +
        "item.[sling:resourceType]  = 'zg/layoutEngine/bootstrapGrid/components/fixedComponent' OR " +
        "item.[sling:resourceType]  = 'zg/layoutEngine/bootstrapGrid/components/metadataContainer' OR " +
        "item.[sling:resourceType]  = 'zg/layoutEngine/bootstrapGrid/components/paragraph')"

process(query, { resource, valueMap ->
    def bootstrap = resource.getChild('styles')?.getChild('bootstrap')
    valueMap = bootstrap?.adaptTo(ModifiableValueMap.class)

    if(valueMap && !valueMap.get(COL_XS, String.class)) {
        valueMap.put(COL_XS, FULL_WIDTH)

        return true
    }

    return false
})