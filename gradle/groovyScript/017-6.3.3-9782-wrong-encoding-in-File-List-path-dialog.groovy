/*
 * ZGCQ-9782 - wrong encoding in File List path dialog
 */

setDryRun false
setActivate false
setSilent true

def contentPath = '/content'
def query = "SELECT * FROM [nt:unstructured] WHERE ISDESCENDANTNODE ([$contentPath]) AND [sling:resourceType] = 'zg/commons/components/data/fileList'"

process(query, { resource, valueMap ->
	def items = []
	if (!resource.hasProperty("items")) {
		resource?.getChild("items")?.getChildren()?.each { r ->
			if(r.valueMap?.value != null) {
				items.add(r.valueMap.value)
				resourceResolver.delete(r)
			}
		}

		setResourceProperties([resource.path], ["items": items.toArray()])

		if (!isDryRun()) {
			resourceResolver.delete(resource.getChild("items"))
			session.save()
		}
	}
})
