import org.apache.commons.lang3.StringUtils

/*
 * ZGCQ-8076 Options and Styles configurations deletion
 */

setDryRun false
setActivate false
setSilent true

def contentPath = "/content"
def configuration = "SELECT * FROM [nt:unstructured] WHERE ISDESCENDANTNODE ([$contentPath]) AND [cq:template] = '/apps/zg/zenGarden/templates/site/siteGroupConfiguration'"

process(configuration, {res, vm ->
	def parent = res.parent
	def options = resourceResolver.getResource(parent.getChild("options")?.getPath())
	removeResource(options)
	def styles = resourceResolver.getResource(parent.getChild("styles-configuration")?.getPath())
	removeResource(styles)
})