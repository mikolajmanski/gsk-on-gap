# To setup instance from backup

1. Run `./gradlew :props`
2. Choose folder for backups
3. Download backup file to the backups folder
4. Run `./gradlew instanceSetup`

# pasat

## pasat - Python-based AEM SysAdmin Toolkit

pasat stands for Python-based AEM SysAdmin Toolkit. This is a library and command-line tool that is used by
Pharma Tech - Digital Marketing Tech AEM Platform team to automate various Adobe Experience Manager activities.
This tool can also be used to assist GSK AEM developers in maintaining their local AEM instances.

## INSTALLATION GUIDE
	
Get latest python 3 executable from here: https://www.python.org/downloads/ and install it
	Create/update PIP configuration file (see https://pip.pypa.io/en/stable/user_guide/#config-file for correct location) with following content:

```text
[global]
index = https://services.gdsgsk.com/nexus/repository/pypi-public/pypi
index-url = https://services.gdsgsk.com/nexus/repository/pypi-public/simple 
```
	
Run below command
`python -m pip install pasat`

To upgrade, run below command
`python -m pip install -U pasat`

## CONFIGURATION

Configuration file is located here %USERPROFILE%\.pasat\pasat.ini or here ~/.pasat/pasat.ini . Location of it can be
overridden using --configuration-file argument.

Configuration file uses following format:

```text
[DEFAULT]
login = aem_login
password = aem_password
[cf65-local-author]
host = localhost
port = 4502
login = admin
password = admin
```

