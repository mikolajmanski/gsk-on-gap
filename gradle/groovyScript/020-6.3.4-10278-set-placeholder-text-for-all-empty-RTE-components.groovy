/*
* ZGCQ-10278 script will make empty RTE to display placeholder text on author.
* Running this this script is optional
*/
import javax.jcr.query.Query

setDryRun false
setActivate false
setSilent true

def PATH = '/content'
def index = 0


def query = "SELECT * FROM [nt:base] WHERE ISDESCENDANTNODE ([$PATH]) AND [sling:resourceType] = 'zg/commons/components/base/richText' AND [text] IS NULL"
resourceResolver.findResources(query, Query.JCR_SQL2).each { resource ->
	def valueMap = resource.adaptTo(ModifiableValueMap.class)
	valueMap.put("text", "<br/>")
	index++
	if (!dryRun && index % 800 == 0) {
		session.save()
	}
}
if (!dryRun) {
	session.save()
}
