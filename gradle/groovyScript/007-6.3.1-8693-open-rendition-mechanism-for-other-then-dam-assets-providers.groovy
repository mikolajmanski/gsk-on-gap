/*
 * ZGCQ-8693 Open rendition mechanism for other then DAM assets providers
 */

import org.apache.jackrabbit.commons.JcrUtils

setDryRun false
setActivate false
setSilent true

def contentPath = "/content"
def query = "SELECT * FROM [nt:unstructured] WHERE ISDESCENDANTNODE ([$contentPath]) AND " +
        "[sling:resourceType] = 'zg/commons/components/rendition/renditionsList'"

process(query, { resource, valueMap ->
    def parent = resource.parent
    if (!dryRun && !parent.isResourceType('zg/commons/components/base/paragraphSystem')) {
        def parsys = JcrUtils.getOrCreateByPath(parent.path + '/parsys', 'nt:unstructured', session)
        parsys.setProperty('sling:resourceType', 'zg/commons/components/base/paragraphSystem')
        session.move(resource.path, parsys.path + '/renditionsList')
        session.move(parsys.path, parent.path + '/renditions')
        session.save()
    }
})
