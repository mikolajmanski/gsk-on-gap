import org.apache.commons.lang3.StringUtils

/*
 * ZGCQ-7518 Textfield format as input type
 */

setDryRun false
setActivate false
setSilent true

def contentPath = "/content"
def query = "SELECT * FROM [nt:unstructured] WHERE ISDESCENDANTNODE ([$contentPath]) AND " +
		"[sling:resourceType] = 'zg/form/components/textField'"

process(query, { resource, valueMap ->
	String format = valueMap.get("format", "")
	if (StringUtils.isBlank(format) || format in ['custom', 'none']) {
		valueMap.put("format", "text")

		// custom regex was ignored for all formats but 'custom',
		// now it will be applied for all formats except 'number' (by w3c rules),
		// so remove this property now to make sure it wasn't set in JCR unused.
		if (format != 'custom') {
			valueMap.remove("customRegex")
		}
		return true
	}
})
