/*
* PES-553 Remove HTML tags from accordion panel names
*/

import org.apache.sling.api.resource.ModifiableValueMap

setDryRun false
setActivate false

def query = "SELECT * FROM [nt:unstructured] WHERE (ISDESCENDANTNODE('/content') OR ISDESCENDANTNODE('/conf')) AND [sling:resourceType]='zg/commons/components/media/accordion'"
def counter = 1

SESSION_SAVE_INTERVAL = 1000

process(query, { resource, valueMap ->
    resource.getChild("names")?.getChildren()?.each { it ->
        def map = it.adaptTo(ModifiableValueMap.class)
        if (map && map.containsKey("name")) {
            def name = map.get("name", "")
            def nameWithoutHtmlTags = name.replaceAll("<[^>]*>", "")

            if (name != nameWithoutHtmlTags) {
                map.put("name", nameWithoutHtmlTags)
                if (!dryRun && shouldSaveSession(counter)) {
                    println "Saving changes after processing ${counter} items"
                    resourceResolver.commit()
                }
                counter++
            }
        }
    }
})

if (!dryRun) {
    resourceResolver.commit()
}

def shouldSaveSession(counter) {
    return counter % SESSION_SAVE_INTERVAL == 0
}
