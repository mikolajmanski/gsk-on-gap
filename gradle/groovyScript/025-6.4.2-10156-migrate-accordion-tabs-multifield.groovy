import java.util.regex.Pattern
import org.apache.commons.lang.StringUtils
import java.util.regex.Matcher

setDryRun false
setActivate false
setSilent true

def wronglyNamedItemsQuery = "SELECT * FROM [nt:unstructured] WHERE [sling:resourceType] = 'zg/commons/components/media/accordion' OR [sling:resourceType] = 'zg/commons/components/base/tabs'"
def findCarouselsQuery = "SELECT * FROM [nt:unstructured] WHERE [sling:resourceType] = 'zg/commons/components/media/carousel'"
Pattern NAME_ITEM_PATTERN = Pattern.compile("item_([0-9]*)")


Iterator<Resource> findWronglyNamedResources = resourceResolver.findResources(wronglyNamedItemsQuery, "sql")
while (findWronglyNamedResources.hasNext()) {
	Resource resource = findWronglyNamedResources.next()
	moveChildren(resource.getChild("names"), NAME_ITEM_PATTERN)
	moveChildren(resource, NAME_ITEM_PATTERN)
	putItemIds(resource.getChild("names"))
	if (!dryRun) {
		session.save()
	}
}
Iterator<Resource> carouselResources = resourceResolver.findResources(findCarouselsQuery, "sql")
while (carouselResources.hasNext()) {
	Resource resource = carouselResources.next()
	putItemIds(resource.getChild("names"))
}

if (!dryRun) {
	session.save()
}

def moveChildren(resource, PATTERN) {
	if (resource != null) {
		Iterator<Resource> children = resource.listChildren()
		while (children.hasNext()) {
			Resource child = children.next()
			String nodeName = child.getName()
			Matcher matcher = PATTERN.matcher(nodeName)
			if (matcher.find()) {
				String counter = matcher.group(1)
				String sourcePath = child.getPath()
				String destinationPath = sourcePath.substring(0, sourcePath.lastIndexOf("_")) + counter
				session.move(sourcePath, destinationPath)
			}
		}
	}
}

def putItemIds(resource) {
	if(resource != null) {
		Iterator<Resource> children = resource.listChildren()
		while (children.hasNext()) {
			Resource child = children.next()
			ValueMap valueMap = child.adaptTo(ModifiableValueMap.class)
			if (valueMap != null && valueMap.get("itemId") == null) {
				valueMap.put("itemId", StringUtils.substringAfterLast(child.getPath(), "/item"))
			}
		}
	}
}
