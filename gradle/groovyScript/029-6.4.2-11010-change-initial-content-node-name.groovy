import javax.jcr.RepositoryException

/*
* ZGCQ-11010 - Initial content node name on ZG templates is changed from @zg_initial to zg_initial
*/

setDryRun false
setActivate false
setSilent true

def contentPath = '/content'
def query = "SELECT * FROM [nt:unstructured] WHERE ISDESCENDANTNODE('$contentPath') AND [sling:resourceType] = 'zg/zenGarden/renderers/pageTemplateRenderer'"

process(query, { resource, valueMap ->
	try {
		def oldNode = getNode(resource.path + "/@zg_initial")

		if (!dryRun) {
			rename oldNode to "zg_initial"
		}
	} catch (RepositoryException e) {
	}
})