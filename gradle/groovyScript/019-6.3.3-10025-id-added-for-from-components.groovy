/*
 * ZGCQ-10025 - Two exact same form on page have the sama ids
 */

setDryRun false
setActivate false
setSilent true

def generatedIdResources = ["zg/form/components/checkboxField", "zg/form/components/radioGroup"] as ArrayList
def nameAsIdResources = ["zg/form/components/fileUpload", "zg/form/components/hiddenField", "zg/form/components/selectField", "zg/form/components/textArea", "zg/form/components/textField"] as ArrayList

def query = "SELECT * FROM [nt:unstructured] WHERE ISDESCENDANTNODE ([/content]) AND [sling:resourceType] LIKE 'zg/form/components/%' AND [sling:resourceType] <> 'zg/form/components/form'"
process(query, { resource, valueMap ->
	if(!valueMap.containsKey("idGenerated") && !valueMap.containsKey("nameAsId")) {
		def resourceType = resource.getResourceType()
		if(generatedIdResources.contains(resourceType)) {
			valueMap.put("idGenerated", "true")
		}
		else if(nameAsIdResources.contains(resourceType)) {
			valueMap.put("nameAsId", "true")
		}
	}
	if(!dryRun) {
		session.save()
	}
})