/*
* ZGCQ-11403 - Sets default header size for Accordion component to prevent unwanted changes when submitting component dialog
*/

setDryRun false
setActivate false
setSilent true

def titleTypePropertyName = "titleType"
def defaultTitleType = "H3"

def query = "SELECT * FROM [nt:unstructured] WHERE (ISDESCENDANTNODE('/content') OR ISDESCENDANTNODE('/conf')) AND [sling:resourceType]='zg/commons/components/media/accordion'"
process(query, {resource, valueMap ->
	if (!valueMap.containsKey(titleTypePropertyName)) {
		valueMap.put(titleTypePropertyName, defaultTitleType)
		return true
	}
})
