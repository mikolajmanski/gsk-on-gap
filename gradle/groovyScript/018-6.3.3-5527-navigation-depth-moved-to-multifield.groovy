/*
 * ZGCQ-5527 - navigation depth moved to multifield
 */

setDryRun false
setActivate false
setSilent true

def contentPath = '/content'
def query = "SELECT * FROM [nt:unstructured] WHERE ISDESCENDANTNODE ([$contentPath]) AND [sling:resourceType] = 'zg/navigation/components/navigation'"

process(query, { navResource, vm ->
	if (vm.depth != null) {
		navResource?.getChild('items')?.getChildren()?.each { r ->
			setResourceProperties([r.path], ['depth': vm.depth])
		}
	}
})

def propertyToRemove = ['depth']
removeExistingProperties(query, propertyToRemove)