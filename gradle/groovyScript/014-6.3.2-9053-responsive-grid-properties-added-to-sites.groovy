/*
 * ZGCQ-9053 Enable Emulator for Layout Engine pages
 */

setDryRun false
setActivate false
setSilent true

def contentPath = "/content"
def query = "SELECT * FROM [nt:unstructured] WHERE ISDESCENDANTNODE ([$contentPath]) AND " +
		"[sling:resourceType] = 'zg/zenGarden/renderers/siteGroupRenderer'"

process(query, { resource, valueMap ->
	info resource.path
	valueMap.put("cq:deviceGroups", ["/etc/mobile/groups/responsive"] as String[])

	def cqResponsiveResource = createResource(resource, "cq:responsive")
	if (cqResponsiveResource != null) {
		def breakpointsResource = createResource(cqResponsiveResource, "breakpoints")
		createResource(breakpointsResource, "phone", "Phone", 768)
		createResource(breakpointsResource, "tablet", "Tablet", 992)
		createResource(breakpointsResource, "desktop", "Desktop", 1200)
	}
})

session.save()
def createResource(parentResource, name) {
	def properties = new HashMap()
	properties.put("jcr:primaryType", "nt:unstructured")
	def result = null
	try {
		result = resourceResolver.create(parentResource, name, properties)
	} catch (PersistenceException e) {
		info "Unable to create " + name+ " node at " + parentResource.path
	}
	return result
}

def createResource(parentResource, name, title, width) {
	def properties = new HashMap()
	properties.put("jcr:primaryType", "nt:unstructured")
	properties.put("title", title)
	properties.put("width", width)
	return resourceResolver.create(parentResource, name, properties)
}

def info(msg) {
	if (!silent) {
		println msg
	}
}