/*
 * ZGCQ-10231 - Changing itemId type from String to Long
 */
import javax.jcr.query.Query

setDryRun true
setActivate false
setSilent true

def contentPath = '/content'
def query = "SELECT item.* " +
        "FROM [nt:unstructured] AS item " +
        "INNER JOIN [nt:unstructured] AS names " +
        "ON ISCHILDNODE(item, names) " +
        "INNER JOIN [nt:unstructured] AS component " +
        "ON ISCHILDNODE(names, component) " +
        "WHERE ISDESCENDANTNODE (component, [$contentPath]) AND " +
        "(component.[sling:resourceType] = 'zg/commons/components/media/accordion' " +
        "OR component.[sling:resourceType] = 'zg/commons/components/base/tabs' " +
        "OR component.[sling:resourceType] = 'zg/commons/components/media/carousel') " +
        "AND NAME(names) = 'names' " +
        "AND NAME(item) like 'item%'"
def itemIdName = "itemId"

Iterator<Resource> findResourcesWithWrongIdType = resourceResolver.findResources(query, Query.JCR_SQL2)
while (findResourcesWithWrongIdType.hasNext()) {
    Resource resource = findResourcesWithWrongIdType.next()
    if(resource != null) {
        ValueMap valueMap = resource.adaptTo(ModifiableValueMap.class)
        def itemId = valueMap.get(itemIdName, Long.class)
        if (!dryRun) {
            if (itemId == null) {
                throw new Exception("Item ID is required for $resource.path resource. " +
                        "Please make sure you applied 6.4.2-10156-migrate-accordion-tabs-multifield script before this one!")
            } else {
                valueMap.put(itemIdName, itemId)
            }
        }
    }
}

if (!dryRun) {
    session.save()
}