/*
* ZGCQ-8675 - Remove empty spaces in allowed templates configuration in site group
*/

setDryRun false // this is the only change
setActivate false

def contentPath = "/content"
def query = "SELECT * FROM [nt:unstructured] WHERE ISDESCENDANTNODE('$contentPath') AND [cq:template] = '/apps/zg/zenGarden/templates/site/siteGroupConfiguration'"

process(query, { resource, valueMap ->
	map = valueMap["cq:allowedTemplates"]
	trimmedPaths = map.collect { it.trim() }
	valueMap["cq:allowedTemplates"] = trimmedPaths.toArray()
})