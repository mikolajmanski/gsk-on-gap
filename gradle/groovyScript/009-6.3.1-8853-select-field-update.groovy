/*
 * ZGCQ-8853 Support for label/value pairs in SelectField
 */

setDryRun false
setActivate false
setSilent true

def contentPath = "/content"
def query = "SELECT * FROM [nt:unstructured] WHERE ISDESCENDANTNODE ([$contentPath]) AND " +
        "[sling:resourceType] = 'zg/form/components/selectField'"

process(query, { resource, valueMap ->
    def options = valueMap.options
    def includeBlankOption = valueMap.includeBlankOption
    def optionsResource = createOptionsResource(resource)
    cleanProperties(valueMap)
    def index = 1
    if (includeBlankOption && includeBlankOption != "false") {
        createOption(optionsResource, "item_" + (index), "&nbsp;", "")
        index++
    }
    options.each { value ->
        createOption(optionsResource, "item_" + (index), value, value)
        index++
    }
})

def createOptionsResource(parentResource) {
    def properties = new HashMap()
    properties.put("jcr:primaryType", "nt:unstructured")

    if(getNode(parentResource.getPath()).hasNode("options") == false) {
        info "Creating options on " + parentResource.getPath()
        return resourceResolver.create(parentResource, "options", properties)
    } else {
        info "Options already exist on " + parentResource.getPath()
        return getResource(parentResource.getPath()+"/options")
    }
}

def createOption(parentResource, name, label, value) {
    def properties = new HashMap()
    properties.put("jcr:primaryType", "nt:unstructured")
    properties.put("label", label)
    properties.put("value", value)
    return resourceResolver.create(parentResource, name, properties)
}

static def cleanProperties(valueMap) {
    valueMap.remove("options")
    valueMap.remove("includeBlankOption")
}

def info(msg) {
    if (!silent) {
        println msg
    }
}