/*
* ZGCQ-11352 - FileList component is extended to show/hide 'Download link label'.
*/

setDryRun false
setActivate false
setSilent true

def showDownloadLabel = "showDownloadLabel"
def appendFileName = "appendFileName"

def query = "SELECT * FROM [nt:unstructured] WHERE ISDESCENDANTNODE('/content') AND [sling:resourceType]='zg/commons/components/data/fileList'"
process(query, {resource, valueMap ->
	String format = valueMap.get(showDownloadLabel, "")
	if (StringUtils.isBlank(format)) {
		valueMap.put(showDownloadLabel, "true")
	}
	String oldPropertyValue = valueMap.get(appendFileName, "")
	valueMap.remove(appendFileName)
	if (!StringUtils.isBlank(oldPropertyValue)) {
		valueMap.put("showFileName", oldPropertyValue)
	}
	return true
})