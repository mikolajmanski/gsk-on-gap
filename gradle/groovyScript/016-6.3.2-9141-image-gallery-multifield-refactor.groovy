/*
 * ZGCQ-9141 Multifield Widget Could Be Populated by Array Pill
 */

setDryRun false
setActivate false
setSilent true

def CONTENT_PATH = "/content"

def query = "SELECT * FROM [nt:unstructured] WHERE ISDESCENDANTNODE('$CONTENT_PATH') AND [sling:resourceType] = 'zg/commons/components/media/imageGallery'"

process(query, { resource, valueMap ->

	def items = []
	if (!resource.hasProperty("items")) {
		resource?.getChild("items")?.getChildren()?.each { r ->
			items.add(r.valueMap.value)
			resourceResolver.delete(r)
		}

		setResourceProperties([resource.path], ["items": items.toArray()])

		if (!isDryRun()) {
			resourceResolver.delete(resource.getChild("items"))
			session.save()
		}
	}
})