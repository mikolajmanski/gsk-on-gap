/**
 * ZGCQ-9059 - Make context item path in Listing a relative path
 */

setDryRun false
setActivate false
setSilent true

def path = '/content'
def query = "SELECT * FROM [nt:unstructured] WHERE ISDESCENDANTNODE([$path]) AND [sling:resourceType]='bridge/components/listing'"

process(query, { resource, valueMap ->
    def deprecatedContextParam = valueMap.get('contextPath')
    def validContextParam = valueMap.get('contextItemId')
    def ModifiableValueMap mvm = resource.adaptTo(ModifiableValueMap.class)

    if (deprecatedContextParam != null) {
        info("* found     contextPath='$deprecatedContextParam'")
    }
    if (validContextParam != null) {
        info("* found     contextItemId='$validContextParam'")
    }
    if (deprecatedContextParam != null && validContextParam == null) {
        updateContext(mvm, deprecatedContextParam)
    }
    if (deprecatedContextParam != null) {
        clearOldContext(mvm)
    }
    info('')
})

def updateContext(mvm, contextPath) {
    def contextItemId = StringUtils.substringAfterLast(contextPath, '@bridgeContext/')
    info("* adding    contextItemId='$contextItemId'")
    mvm.put('contextItemId', contextItemId)
}


def clearOldContext(mvm) {
    info("* removing  contextPath")
    mvm.remove('contextPath')
}

if (!dryRun){
    session.save()
    info("Changes saved.")
} else {
    info("Changes reverted.")
}

def info(msg) {
    if (!silent) {
        println msg
    }
}
