/*
* ZGCQ-11548 Lorem ipsum placeholder visible on publish
* migration script for replacing <br/> from richtext to <ph/>
*/

setDryRun false // this is the only change
setActivate false

RICHTEXT_RESOURCE_TYPE = 'zg/commons/components/base/richText'

def query = "SELECT * FROM [nt:unstructured] WHERE ISDESCENDANTNODE('/content') AND [sling:resourceType]='$RICHTEXT_RESOURCE_TYPE' AND [text] = '<br/>'"

def propertyMap = ["text":"<ph/>"]
setResourceProperties(query, propertyMap, true)
