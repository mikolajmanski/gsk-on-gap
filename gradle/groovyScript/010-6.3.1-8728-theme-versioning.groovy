/*
 * ZGCQ-8728 Theme versioning
 */

setDryRun false
setActivate false
setSilent true

def designPath = "/etc/designs/zg"
def themeQUERY = "SELECT * FROM [cq:PageContent] WHERE ISDESCENDANTNODE ([$designPath]) AND " +
		"[sling:resourceType] = 'zg/theme/renderers/themeRenderer'"

process(themeQUERY, { resource, valueMap ->
	def defaultVersion = "0-0-1"
	def theme = resource
	def themeVersion = theme.valueMap["version"]
	if (themeVersion == null) {
		def modifiableValueMap = theme.adaptTo(ModifiableValueMap.class)
		modifiableValueMap.put("version", defaultVersion)
	}
})


if (!dryRun) {
	if (!silent) {
		println "Changes saved"
	}
	session.save()
}