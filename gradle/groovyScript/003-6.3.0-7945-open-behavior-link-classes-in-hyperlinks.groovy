import javax.jcr.query.Query

/*
 * ZGCQ-7945 replaces hyperlink targetblank property with openBehavior and variants with linkClasses.
 */

setDryRun false
setActivate false
setSilent true

def index = 0
def contentPath = "/content"
def sitegroups = "SELECT * FROM [nt:base] as s WHERE ISDESCENDANTNODE([$contentPath]) " +
		"AND (s.[sling:resourceType] = 'zg/zenGarden/renderers/siteGroupRenderer' OR s.[sling:resourceType] = 'zg/zenGarden/renderers/mobileGroupRenderer')"

resourceResolver.findResources(sitegroups, Query.JCR_SQL2).each { resource ->
	def parentPath = resource.getParent().getPath()
	def options = getResource(parentPath + "/configuration/styles-configuration/hyperlink-predefined-styles/jcr:content/cssClasses/options")
	def variantsConfiguration = [:]
	options?.getChildren().each { r ->
		variantsConfiguration.put(r.id, r.value)
	}

	if (!variantsConfiguration.isEmpty()) {
		def components = "SELECT * FROM [nt:base] as s WHERE ISDESCENDANTNODE([$parentPath]) " +
				"AND (s.[sling:resourceType] = 'zg/commons/components/base/image' OR s.[sling:resourceType] = 'zg/commons/components/base/box' OR s.[sling:resourceType] = 'zg/commons/components/media/carouselSlide')"

		resourceResolver.findResources(components, Query.JCR_SQL2).each { componentResource ->
			def hyperlink = componentResource.getChild("hyperlink")
			if (hyperlink != null) {
				replaceVariants(hyperlink, variantsConfiguration)
				replaceTargetBlank(hyperlink)
				index++
			}
		}
		if(!dryRun && index % 800 == 0){
			session.save()
		}
	}
	if(!dryRun) {
		session.save()
	}
}

def replaceVariants(hyperlink, variantsConfiguration) {
	def valueMap = hyperlink.adaptTo(ModifiableValueMap.class)
	if(valueMap != null) {
		String[] variants = valueMap.get("variants", String[].class)
		if (variants != null && variants[0] != "") {
			List<String> list = new ArrayList(Arrays.asList(variants))
			ListIterator<String> lit = list.listIterator()
			while (lit.hasNext()) {
				def variantId = lit.next()
				def cssClass = variantsConfiguration[variantId]
				if (cssClass == "lightbox") {
					hyperlink.openBehavior = "overlay"
					lit.remove()
				} else if (cssClass != "null") {
					lit.set(cssClass)
				}
			}
			if (!list.isEmpty()) {
				hyperlink.linkClasses = StringUtils.join(list, " ")
			}
		}
		if (valueMap.get("variants") != null) {
			valueMap.remove("variants")
		}
	}
	else {
		info hyperlink.path
	}
}

def replaceTargetBlank(hyperlink) {
	def valueMap = hyperlink.adaptTo(ModifiableValueMap.class)
	if (valueMap != null) {
		if (hyperlink.openBehavior == null) {
			String targetBlank = valueMap.get("targetBlank", String.class)
			if (StringUtils.isNotBlank(targetBlank)) {
				if (StringUtils.equals(targetBlank, "true")) {
					hyperlink.openBehavior = "newWindow"
				} else {
					hyperlink.openBehavior = "sameTab"
				}
			}
		}
		if (valueMap.get("targetBlank") != null) {
			valueMap.remove("targetBlank")
		}
	}
	else {
		info hyperlink.path
	}
}

def info(msg) {
	if (!silent) {
		log.info msg
		println msg
	}
}
